import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KosarService {

  termekek: any=[];
  constructor(  ) {this.getTotalPrice() }

  addTermek(key:any, db:any, ar:any){
    let body:any={};
    body.novenyKey = key;
    body.db = db;
    body.ar = (db * ar);
    this.termekek.push(body);
    this.getTotalPrice();
  }
  getTotalPrice() : number{
    let grandTotal = 0;
    this.termekek.map((a:any)=>{
      grandTotal += a.ar;
    })
    return grandTotal;
  }


  getTermekek(){
    return this.termekek;
  }

  deleteTermekek(){
   this.termekek = [];
  }

  torolTermek(termek:any){
    this.termekek = this.termekek.filter(
      (e:any) => e!= termek
    )
  }

}
