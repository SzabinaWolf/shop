import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/compat/database';
import { Noveny } from '../models/noveny';
import { Kosar } from '../models/kosar';

@Injectable({
  providedIn: 'root'
})
export class FireService {

  novenyRef: AngularFireList<Noveny>;
rendelesRef: AngularFireList<Kosar>;


  constructor(
    private fire: AngularFireDatabase
  ) { 
    this.novenyRef = this.fire.list('/novenyek');
    this.rendelesRef = this.fire.list('/rendeles');
  }

  getNovenyek(){
    return this.novenyRef;
  }

  addRendeles(body:any){
return this.rendelesRef.push(body);
  }
}
