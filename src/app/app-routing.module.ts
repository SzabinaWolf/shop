import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RolunkComponent } from './reszek/rolunk/rolunk.component';
import { NovenyekComponent } from './reszek/novenyek/novenyek.component';
import { RendelesComponent } from './reszek/rendeles/rendeles.component';

const routes: Routes = [
  {path: "rolunk", component: RolunkComponent},
  {path: "novenyek", component: NovenyekComponent},
  {path: "rendeles", component: RendelesComponent},
  {path: "", redirectTo: "/rolunk", pathMatch: "full"},
  {path: "**", redirectTo: "/rolunk", pathMatch: "full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
