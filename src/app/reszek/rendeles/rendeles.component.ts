import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';
import { Noveny } from 'src/app/models/noveny';
import { FireService } from 'src/app/services/fire.service';
import { KosarService } from 'src/app/services/kosar.service';

@Component({
  selector: 'app-rendeles',
  templateUrl: './rendeles.component.html',
  styleUrls: ['./rendeles.component.css']
})
export class RendelesComponent {

  megrendeles: any = {};
  termekek: any;
  novenyek: any = [];
public grandTotal!: number;

  constructor(
    private fire: FireService,
    private kosar: KosarService,
    private router: Router) {
    this.termekek = this.kosar.getTermekek();
 
    this.fire.getNovenyek().snapshotChanges().pipe(map((changes) => changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))))
      .subscribe((adatok) => {
        this.novenyek = adatok;
        this.grandTotal =  this.kosar.getTotalPrice();
      });
      console.log(this.termekek);

  }

  torol(termek: any) {
    this.kosar.torolTermek(termek);
    this.termekek = this.kosar.getTermekek();
    this.grandTotal = this.kosar.getTotalPrice();

  }

  ment() {
    this.megrendeles.rendeles = this.termekek;
    this.megrendeles.status = false;
    this.megrendeles.datum = String(Date());
    this.fire.addRendeles(this.megrendeles);
    this.grandTotal = this.kosar.getTotalPrice();
    this.router.navigate(['/novenyek']);
    console.log(this.megrendeles);

  }

 keresNev(key: string) {
    if (this.novenyek.length == 0) return null;
    return this.novenyek.filter(
      (a: any) => a.key == key
    )[0].megnevezes
  } 

deleteKosar(){
  this.termekek = [];
  this.grandTotal = 0;

}
}
