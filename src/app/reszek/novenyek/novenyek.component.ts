import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';
import { FireService } from 'src/app/services/fire.service';
import { KosarService } from 'src/app/services/kosar.service';

@Component({
  selector: 'app-novenyek',
  templateUrl: './novenyek.component.html',
  styleUrls: ['./novenyek.component.css']
})
export class NovenyekComponent {

  novenyek: any;
  key: any;
  db: any;

  constructor(
    private fire: FireService,
    private kosar: KosarService,
    private router: Router
  ){
this.getNovenyek();
  }

  getNovenyek(): void {
    this.fire.getNovenyek().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ key: c.payload.key, ...c.payload.val() })
        )
      )
    ).subscribe(data => {
      this.novenyek = data;
    });
  }


  addTermek(key:any, db:any, ar:any){
    this.kosar.addTermek(key, db, ar);   
  }
}
