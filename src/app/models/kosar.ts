export class Kosar {
    key?: string;
    nev?: string;
    cim?: string;
    datum?: string;
    status?: boolean;
    rendeles?:{
        novenyKey?: string;
        darab?: number;
        ar?: number;
    }[]
}
